package com.dto;

import java.sql.Timestamp;

public class MsgprsDto {
    private Integer id;
    private String name;
    private String retype;
    private Integer msgId;
    private Timestamp stime;
    private Timestamp endtime;
    private String plugin;
    private Boolean enable;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRetype() {
        return retype;
    }
    public void setRetype(String retype) {
        this.retype = retype;
    }
    public Integer getMsgId() {
        return msgId;
    }
    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }
    public Timestamp getStime() {
        return stime;
    }
    public void setStime(Timestamp stime) {
        this.stime = stime;
    }
    public Timestamp getEndtime() {
        return endtime;
    }
    public void setEndtime(Timestamp endtime) {
        this.endtime = endtime;
    }
    public String getPlugin() {
        return plugin;
    }
    public void setPlugin(String plugin) {
        this.plugin = plugin;
    }
    public Boolean getEnable() {
        return enable;
    }
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
    
}
