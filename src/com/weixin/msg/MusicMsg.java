package com.weixin.msg;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.weixin.vo.Music;

@XmlRootElement(name="xml")
public class MusicMsg extends Msg {
	private Music music;
	public MusicMsg() {
		// TODO Auto-generated constructor stub
		this.msgType=MsgEmum.music.toString();
	}
	@XmlElement(name="Music")
	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}
}
