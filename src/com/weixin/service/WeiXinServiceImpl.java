package com.weixin.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSONObject;
import com.dto.Signature;
import com.util.SHA1Util;
import com.weixin.exception.WeixinException;
import com.weixin.util.HttpUtil;
import com.weixin.vo.Article;
import com.weixin.vo.QunArticle;

/**
 * 
 * 微信接口的实现类 ，使用的时候  可以直接放到spring实例池里面，
 * 在需要的时候 直接注入进去即可。
 * 
 * WeiXinServiceImpl.java
 * 
 * @author microxdd
 * @version 创建时间：2014 2014年9月15日 下午4:09:35 micrxdd
 * 
 */
public class WeiXinServiceImpl implements WeiXinService {
    // 需要注入
    private static Properties properties=new Properties();
    // private Logger log = Logger.getLogger(WeixinUtil.class);
    
    static {
	try {
	    properties.load(WeiXinServiceImpl.class.getResourceAsStream("/wei.properties"));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
    
    
    
    
    
    /**
     * 检验微信签名，验证消息的真实性
     * 
     * @param signature
     * @return JSONObject 消息的真实性
     */
    @Override
    public boolean ChackSignature(Signature signature) {
	if (signature != null && signature.getSignature() != null) {
	    List<String> params = new ArrayList<String>();
	    params.add(get("Token"));
	    params.add(signature.getTimestamp());
	    params.add(signature.getNonce());
	    Collections.sort(params, new Comparator<String>() {
		public int compare(String o1, String o2) {
		    return o1.compareTo(o2);
		}
	    });
	    // 2. 将三个参数字符串拼接成一个字符串进行sha1加密
	    String temp = SHA1Util.encode(params.get(0) + params.get(1)
		    + params.get(2));
	    System.out.println(temp);
	    if (temp.equals(signature.getSignature())) {
		return true;
	    }
	}
	return false;

    }

    // 获取tokenURL
    public static final String URL_ACCESSTOKEN = "https://api.weixin.qq.com/cgi-bin/token?";

    // 创建菜单URL
    public static final String URL_MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";

    // 获取菜单URL
    public static final String URL_MENU_GET = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=";

    // 删除菜单URL
    public static final String URL_MENU_DELETE = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=";

    // 获取access_token的时间
    private long getTime=System.currentTimeMillis();

    // 当前获取的access_token(不用每次获取)
    private String access_token="1CXUdW4z0hR4tCs1lapOS9KexEhioC99ixX4Qxo5wxH9old_56EroZy6L7xRGHKxCJ9lxrrPbI4Gn7f-Lmr2WQ";
 // 当前获取的access_token(不用每次获取)
   // private String access_token;

    /**
     * 获取access_token
     * 
     * @return JSONObject access_token
     * @throws WeixinException
     */
    @Override
    public String getAccessToken() throws WeixinException {
	if (null != access_token) {// 已经获取了access_token
	    long currentTime = System.currentTimeMillis();
	    if ((currentTime - getTime) < 7200000) {// 过期了 |
		// access_token有效期为7200秒
		return access_token;
	    }
	}

	// 从服务端从新获取access_token
	String url = URL_ACCESSTOKEN + "grant_type="
		+ get("grant_type") + "&appid=" + get("appID")
		+ "&secret=" + get("appsecret");

	String result = HttpUtil.Httpsget(url);
	getTime = System.currentTimeMillis();
	JSONObject obj = JSONObject.parseObject(result);
	access_token = obj.getString("access_token");
	if (null == access_token) {// 错误
	    throw new WeixinException(result);
	}
	System.out.println(access_token);
	return access_token;
    }

    /**
     * 创建菜单
     * 
     * @param json
     * @throws WeixinException
     */
    @Override
    public void createMenu(String json) throws WeixinException {
	String url = URL_MENU_CREATE + getAccessToken();
	String result = HttpUtil.Httpspost(url, json);
	JSONObject obj = JSONObject.parseObject(result);
	int errcode = obj.getIntValue("errcode");
	if (errcode > 0) {
	    throw new WeixinException(result);
	}
    }

    /**
     * 查询菜单
     * 
     * @return JSONObject String json
     * @throws WeixinException
     */
    @Override
    public JSONObject findMenu() throws WeixinException {
	String result = HttpUtil.Httpsget(URL_MENU_GET + getAccessToken());
	return isSuccess(result);
    }

    /**
     * 删除菜单
     * 
     * @throws WeixinException
     */
    @Override
    public void deleteMenu() throws WeixinException {
	String url = URL_MENU_DELETE + getAccessToken();
	String result = HttpUtil.Httpsget(url);
	JSONObject obj = JSONObject.parseObject(result);
	int errcode = obj.getIntValue("errcode");
	if (errcode > 0) {
	    throw new WeixinException(result);
	}
    }

    // 创建用户组
    private final String GROUP_CREATE_URI = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=";
    // 获取用户组
    private final String GROUP_GET_URI = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=";
    // 根据id获取
    private final String GROUP_GETID_URI = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=";
    // 更新用户组
    private final String GROUP_UPDATE_URI = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=";
    private final String GROUP_MEMBERS_UPDATE_URI = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=";

    /**
     * 创建用户分组
     * 
     * @param groupname
     *            组名称
     * @return JSONObject JSONObject
     * @throws WeixinException
     */
    public JSONObject createGroup(String groupname)
	    throws WeixinException {
	Map<String, Object> group = new HashMap<String, Object>();
	Map<String, Object> nameObj = new HashMap<String, Object>();
	nameObj.put("name", groupname);
	group.put("group", nameObj);
	String data = JSONObject.toJSONString(group);
	String result = HttpUtil.Httpspost(GROUP_CREATE_URI + getAccessToken(),
		data);
	JSONObject object = JSONObject.parseObject(result);
	if (object.getIntValue("errcode") != 0) {
	    throw new WeixinException(result);
	}
	return object;
    }

    /**
     * 获取所有的分组
     * 
     * @return JSONObject JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject findAllGroups() throws WeixinException {
	String result = HttpUtil.Httpsget(GROUP_GET_URI + getAccessToken());
	return isSuccess(result);
    }

    /**
     * 根据openid查找用户
     * 
     * @param accessToken
     * @param openid
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject findUserGroup(String openid) throws WeixinException {
	String result = HttpUtil.Httpsget(GROUP_GETID_URI + getAccessToken()
		+ "{\"openid\":\"" + openid + "\"}");
	return isSuccess(result);
    }

    /**
     * 修改分组名
     * 
     * @param accessToken
     * @param id
     *            分组id，由微信分配
     * @param name
     *            分组名字（30个字符以内）
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject updateName(Integer id, String name)
	    throws WeixinException {
	Map<String, Object> group = new HashMap<String, Object>();
	Map<String, Object> nameObj = new HashMap<String, Object>();
	nameObj.put("name", name);
	nameObj.put("id", id);
	group.put("group", nameObj);
	String post = JSONObject.toJSONString(group);
	String result = HttpUtil.Httpspost(GROUP_UPDATE_URI + getAccessToken(),
		post);
	return isSuccess(result);

    }

    /**
     * 移动用户分组
     * 
     * @param accessToken
     * @param openid
     *            用户唯一标识符
     * @param to_groupid
     *            分组id
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject membersMove(String openid, String to_groupid)
	    throws WeixinException {
	String result = HttpUtil.Httpspost(GROUP_MEMBERS_UPDATE_URI
		+ getAccessToken(), "{\"openid\":\"" + openid
		+ "\",\"to_groupid\":" + to_groupid + "}");
	return isSuccess(result);
    }

    /**
     * 判断数据是否提交成功
     * 
     * @param result
     * @return JSONObject
     * @throws WeixinException
     */
    public JSONObject isSuccess(String result) throws WeixinException {
	JSONObject object = JSONObject.parseObject(result);
	if (object.getIntValue("errcode") != 0) {
	    throw new WeixinException(result);
	}
	return object;
    }

    private final String MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=";
    private final String UPLOADNEWS_URL = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=";
    private final String MASS_SENDALL_URL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=";
    private final String MASS_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=";
    private final String MASS_DELETE_URL = "https://api.weixin.qq.com//cgi-bin/message/mass/delete?access_token=";

    /**
     * 发送客服消息
     * 
     * @param message
     * @return JSONObject
     * @throws WeixinException
     */
    private String sendMsg(Map<String, Object> message) throws WeixinException {
	String result = HttpUtil.Httpspost(MESSAGE_URL + getAccessToken(),
		JSONObject.toJSONString(message));
	return result;
    }

    /**
     * 发送文本客服消息
     * 
     * @param openId
     * @param text
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public String sendText(String openId, String text) throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	Map<String, Object> textObj = new HashMap<String, Object>();
	textObj.put("content", text);
	json.put("touser", openId);
	json.put("msgtype", "text");
	json.put("text", textObj);
	String result = sendMsg(json);
	return result;
    }

    /**
     * 发送图片消息
     * 
     * @param openId
     * @param media_id
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public String SendImage(String openId, String media_id) throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	Map<String, Object> textObj = new HashMap<String, Object>();
	textObj.put("media_id", media_id);
	json.put("touser", openId);
	json.put("msgtype", "image");
	json.put("image", textObj);
	String result = sendMsg(json);
	return result;
    }

    /**
     * 发送语音回复
     * 
     * @param openId
     * @param media_id
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public String SendVoice(String openId, String media_id) throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	Map<String, Object> textObj = new HashMap<String, Object>();
	textObj.put("media_id", media_id);
	json.put("touser", openId);
	json.put("msgtype", "voice");
	json.put("voice", textObj);
	String result = sendMsg(json);
	return result;
    }

    /**
     * 发送视频回复
     * 
     * @param openId
     * @param media_id
     * @param title
     * @param description
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public String SendVideo(String openId, String media_id, String title,
	    String description) throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	Map<String, Object> textObj = new HashMap<String, Object>();
	textObj.put("media_id", media_id);
	textObj.put("title", title);
	textObj.put("description", description);

	json.put("touser", openId);
	json.put("msgtype", "video");
	json.put("video", textObj);
	String result = sendMsg(json);
	return result;
    }

    /**
     * 发送音乐回复
     * 
     * @param openId
     * @param musicurl
     * @param hqmusicurl
     * @param thumb_media_id
     * @param title
     * @param description
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public String SendMusic(String openId, String musicurl, String hqmusicurl,
	    String thumb_media_id, String title, String description)
	    throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	Map<String, Object> textObj = new HashMap<String, Object>();
	textObj.put("musicurl", musicurl);
	textObj.put("hqmusicurl", hqmusicurl);
	textObj.put("thumb_media_id", thumb_media_id);
	textObj.put("title", title);
	textObj.put("description", description);

	json.put("touser", openId);
	json.put("msgtype", "music");
	json.put("music", textObj);
	String result = sendMsg(json);
	return result;
    }

    /**
     * 发送图文回复
     * 
     * @param openId
     * @param articles
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public String SendNews(String openId, List<Article> articles)
	    throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	json.put("touser", openId);
	json.put("msgtype", "news");
	json.put("news", articles);
	String result = sendMsg(json);
	return result;
    }

    /**
     * 上传图文消息素材
     * 
     * @param articles
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject uploadnews(List<QunArticle> articles)
	    throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	json.put("articles", articles);
	String result = HttpUtil.Httpspost(UPLOADNEWS_URL + getAccessToken(),
		JSONObject.toJSONString(json));
	return isSuccess(result);
    }

    /**
     * 根据分组进行群发
     * 
     * @param groupId
     * @param mpNewsMediaId
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject massSendall(String groupId, String mpNewsMediaId)
	    throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	Map<String, Object> filter = new HashMap<String, Object>();
	Map<String, Object> mpnews = new HashMap<String, Object>();
	filter.put("group_id", groupId);
	mpnews.put("media_id", mpNewsMediaId);

	json.put("mpnews", mpnews);
	json.put("filter", filter);
	json.put("msgtype", "mpnews");
	String result = HttpUtil.Httpspost(MASS_SENDALL_URL + getAccessToken(),
		JSONObject.toJSONString(json));
	return isSuccess(result);
    }

    /**
     * 根据OpenID列表群发
     * 
     * @param openids
     * @param mpNewsMediaId
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject massSend(String[] openids, String mpNewsMediaId)
	    throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	Map<String, Object> mpnews = new HashMap<String, Object>();
	mpnews.put("media_id", mpNewsMediaId);
	json.put("touser", openids);
	json.put("mpnews", mpnews);
	json.put("msgtype", "mpnews");
	String result = HttpUtil.Httpspost(MASS_SEND_URL + getAccessToken(),
		JSONObject.toJSONString(json));
	return isSuccess(result);
    }

    /**
     * 删除群发
     * 
     * @param msgid
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject massSend(String msgid) throws WeixinException {
	Map<String, Object> json = new HashMap<String, Object>();
	json.put("msgid", msgid);
	String result = HttpUtil.Httpspost(MASS_DELETE_URL + getAccessToken(),
		JSONObject.toJSONString(json));
	return isSuccess(result);
    }

    private final String USER_INFO_URI = "https://api.weixin.qq.com/cgi-bin/user/info?lang=zh_CN&access_token=";
    private final String USER_GET_URI = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=";

    /**
     * 拉取用户信息
     * 
     * @param openid
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject getUserInfo(String openid) throws WeixinException {
	String result = HttpUtil.Httpsget(USER_INFO_URI + getAccessToken()
		+ "&openid=" + openid);
	return isSuccess(result);
    }

    /**
     * 获取帐号的关注者列表
     * 
     * @param next_openid
     * @return JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject getFollwersList(String next_openid)
	    throws WeixinException {
	String result = HttpUtil.Httpsget(USER_GET_URI + getAccessToken()
		+ "&next_openid=" + next_openid+"&count=10000");
	return isSuccess(result);
    }

    private final String QRCOD_CREATE = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=";

    private final String QRCOD_SHOW = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=";

    /**
     * 创建临时二维码
     * 
     * @param expireSeconds
     *            最大不超过1800
     * @param sceneId
     *            场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
     * @return JSONObject JSONObject
     * @throws WeixinException
     */
    @Override
    public JSONObject createScene(int expireSeconds, int sceneId)
	    throws WeixinException {
	Map<String, Object> params = new HashMap<String, Object>();
	Map<String, Object> actionInfo = new HashMap<String, Object>();
	Map<String, Object> scene = new HashMap<String, Object>();
	params.put("expire_seconds", expireSeconds);
	params.put("action_name", "QR_SCENE");
	scene.put("scene_id", sceneId);
	actionInfo.put("scene", scene);
	params.put("action_info", actionInfo);
	String post = JSONObject.toJSONString(params);
	String result = HttpUtil.Httpspost(QRCOD_CREATE + getAccessToken(),
		post);
	return isSuccess(result);
    }

    /**
     * 创建永久二维码
     * 
     * @param sceneId
     * @return
     * @throws WeixinException
     */
    public JSONObject createLimitScene(int sceneId) throws WeixinException {
	Map<String, Object> params = new HashMap<String, Object>();
	Map<String, Object> actionInfo = new HashMap<String, Object>();
	Map<String, Object> scene = new HashMap<String, Object>();
	params.put("action_name", "QR_LIMIT_SCENE");
	scene.put("scene_id", sceneId);
	actionInfo.put("scene", scene);
	params.put("action_info", actionInfo);
	String post = JSONObject.toJSONString(params);
	String result = HttpUtil.Httpspost(QRCOD_CREATE + getAccessToken(),
		post);
	return isSuccess(result);
    }

    /**
     * 获取查看二维码链接
     * 
     * @param ticket
     * @return StringUrl
     */
    @Override
    public String showqrcodeUrl(String ticket) {
	return QRCOD_SHOW.concat(ticket);
    }

    // 发货通知接口
    private final String DELIVERNOTIFY_URL = "https://api.weixin.qq.com/pay/delivernotify?access_token=";

    /**
     * 参与 paySign 签名的字段包括：appid、timestamp、noncestr、package 以及 appkey。 这里
     * signType 并不参与签名微信的Package参数
     * 
     * @param params
     * @return
     * @throws UnsupportedEncodingException
     */
    public String getPackage(Map<String, String> params)
	    throws UnsupportedEncodingException {
	String partnerKey = get("partnerKey");
	String partnerId = get("partnerId");
	String notifyUrl = get("notify_url");
	// 公共参数
	params.put("bank_type", "WX");
	params.put("attach", "yongle");
	params.put("partner", partnerId);
	params.put("notify_url", notifyUrl);
	params.put("input_charset", "UTF-8");
	return packageSign(params, partnerKey);
    }

    /**
     * 构造签名
     * 
     * @param params
     * @param encode
     * @return
     * @throws UnsupportedEncodingException
     */
    public String createSign(Map<String, String> params, boolean encode)
	    throws UnsupportedEncodingException {
	Set<String> keysSet = params.keySet();
	Object[] keys = keysSet.toArray();
	Arrays.sort(keys);
	StringBuffer temp = new StringBuffer();
	boolean first = true;
	for (Object key : keys) {
	    if (first) {
		first = false;
	    } else {
		temp.append("&");
	    }
	    temp.append(key).append("=");
	    Object value = params.get(key);
	    String valueString = "";
	    if (null != value) {
		valueString = value.toString();
	    }
	    if (encode) {
		temp.append(URLEncoder.encode(valueString, "UTF-8"));
	    } else {
		temp.append(valueString);
	    }
	}
	return temp.toString();
    }

    /**
     * 构造package, 这是我见到的最草蛋的加密，尼玛文档还有错
     * 
     * @param params
     * @param paternerKey
     * @return
     * @throws UnsupportedEncodingException
     */
    private String packageSign(Map<String, String> params,
	    String paternerKey) throws UnsupportedEncodingException {
	String string1 = createSign(params, false);
	String stringSignTemp = string1 + "&key=" + paternerKey;
	String signValue = DigestUtils.md5Hex(stringSignTemp).toUpperCase();
	String string2 = createSign(params, true);
	return string2 + "&sign=" + signValue;
    }

    /**
     * 支付签名
     * 
     * @param timestamp
     * @param noncestr
     * @param packages
     * @return
     * @throws UnsupportedEncodingException
     */
    public String paySign(String timestamp, String noncestr,
	    String packages) throws UnsupportedEncodingException {
	Map<String, String> paras = new HashMap<String, String>();
	paras.put("appid", get("appID"));
	paras.put("timestamp", timestamp);
	paras.put("noncestr", noncestr);
	paras.put("package", packages);
	paras.put("appkey", get("paySignKey"));
	// appid、timestamp、noncestr、package 以及 appkey。
	String string1 = createSign(paras, false);
	String paySign = DigestUtils.sha1Hex(string1);
	return paySign;
    }

    /**
     * 支付回调校验签名
     * 
     * @param timestamp
     * @param noncestr
     * @param openid
     * @param issubscribe
     * @param appsignature
     * @return
     * @throws UnsupportedEncodingException
     */
    public boolean verifySign(long timestamp, String noncestr,
	    String openid, int issubscribe, String appsignature)
	    throws UnsupportedEncodingException {
	Map<String, String> paras = new HashMap<String, String>();
	paras.put("appid", get("appID"));
	paras.put("appkey", get("paySignKey"));
	paras.put("timestamp", String.valueOf(timestamp));
	paras.put("noncestr", noncestr);
	paras.put("openid", openid);
	paras.put("issubscribe", String.valueOf(issubscribe));
	// appid、appkey、productid、timestamp、noncestr、openid、issubscribe
	String string1 = createSign(paras, false);
	String paySign = DigestUtils.sha1Hex(string1);
	return paySign.equalsIgnoreCase(appsignature);
    }

    /**
     * 发货通知签名
     * 
     * @param paras
     * @return
     * @throws UnsupportedEncodingException
     * 
     * @参数 appid、appkey、openid、transid、out_trade_no、deliver_timestamp、
     *     deliver_status、deliver_msg；
     */
    private String deliverSign(Map<String, String> paras)
	    throws UnsupportedEncodingException {
	paras.put("appkey", get("paySignKey"));
	String string1 = createSign(paras, false);
	String paySign = DigestUtils.sha1Hex(string1);
	return paySign;
    }

    /**
     * 发货通知
     * 
     * @param access_token
     * @param openid
     * @param transid
     * @param out_trade_no
     * @return
     * @throws WeixinException
     * @throws UnsupportedEncodingException
     */

    public boolean delivernotify(String openid, String transid,
	    String out_trade_no) throws WeixinException,
	    UnsupportedEncodingException {
	Map<String, String> paras = new HashMap<String, String>();
	paras.put("appid", get("appID"));
	paras.put("openid", openid);
	paras.put("transid", transid);
	paras.put("out_trade_no", out_trade_no);
	paras.put("deliver_timestamp", (System.currentTimeMillis() / 1000) + "");
	paras.put("deliver_status", "1");
	paras.put("deliver_msg", "ok");
	// 签名
	String app_signature = deliverSign(paras);
	paras.put("app_signature", app_signature);
	paras.put("sign_method", "sha1");
	String result = HttpUtil.Httpspost(
		DELIVERNOTIFY_URL + getAccessToken(),
		JSONObject.toJSONString(paras));
	isSuccess(result);
	return false;
    }

    @Override
    public String get(String key) {
	return properties.getProperty(key);
    }
    private final String FILEUPLOAD="http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=";
    @Override
    public JSONObject fileUpload(File file, String type) throws WeixinException {
	// TODO Auto-generated method stub
	String result=HttpUtil.FileUpload(FILEUPLOAD+getAccessToken()+"&type="+type, file);
	System.out.println(result);
	JSONObject object= JSONObject.parseObject(result);
	isSuccess(result);
	return object;
    }
    

}
