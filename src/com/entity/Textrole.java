package com.entity;

/**
 * Textrole entity. @author MyEclipse Persistence Tools
 */

public class Textrole implements java.io.Serializable {

    // Fields

    private Integer id;
    private Msgprs msgprs;
    private String txtkey;
    private Boolean isstart;
    private Boolean enable;

    // Constructors

    /** default constructor */
    public Textrole() {
    }

    /** full constructor */
    public Textrole(Msgprs msgprs, String txtkey, Boolean isstart,
	    Boolean enable) {
	this.msgprs = msgprs;
	this.txtkey = txtkey;
	this.isstart = isstart;
	this.enable = enable;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Msgprs getMsgprs() {
	return this.msgprs;
    }

    public void setMsgprs(Msgprs msgprs) {
	this.msgprs = msgprs;
    }

    public String getTxtkey() {
	return this.txtkey;
    }

    public void setTxtkey(String txtkey) {
	this.txtkey = txtkey;
    }

    public Boolean getIsstart() {
	return this.isstart;
    }

    public void setIsstart(Boolean isstart) {
	this.isstart = isstart;
    }

    public Boolean getEnable() {
	return this.enable;
    }

    public void setEnable(Boolean enable) {
	this.enable = enable;
    }

}