package com.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Newsitem entity. @author MyEclipse Persistence Tools
 */

public class Newsitem implements java.io.Serializable {

    // Fields

    private Integer id;
    private Newstype newstype;
    private User user;
    private Timestamp ctime;
    private String description;
    private Set newses = new HashSet(0);

    // Constructors

    /** default constructor */
    public Newsitem() {
    }

    /** full constructor */
    public Newsitem(Newstype newstype, User user, Timestamp ctime,
	    String description, Set newses) {
	this.newstype = newstype;
	this.user = user;
	this.ctime = ctime;
	this.description = description;
	this.newses = newses;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Newstype getNewstype() {
	return this.newstype;
    }

    public void setNewstype(Newstype newstype) {
	this.newstype = newstype;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public Timestamp getCtime() {
	return this.ctime;
    }

    public void setCtime(Timestamp ctime) {
	this.ctime = ctime;
    }

    public String getDescription() {
	return this.description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Set getNewses() {
	return this.newses;
    }

    public void setNewses(Set newses) {
	this.newses = newses;
    }

}