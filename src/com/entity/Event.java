package com.entity;

import java.sql.Timestamp;

/**
 * Event entity. @author MyEclipse Persistence Tools
 */

public class Event implements java.io.Serializable {

    // Fields

    private Integer id;
    private String event;
    private String descri;
    private String retype;
    private Integer msgId;
    private String eventkey;
    private Timestamp ctime;
    private String plugin;
    private Boolean enable;

    // Constructors

    /** default constructor */
    public Event() {
    }

    /** minimal constructor */
    public Event(String event) {
	this.event = event;
    }

    /** full constructor */
    public Event(String event, String descri, String retype, Integer msgId,
	    String eventkey, Timestamp ctime, String plugin, Boolean enable) {
	this.event = event;
	this.descri = descri;
	this.retype = retype;
	this.msgId = msgId;
	this.eventkey = eventkey;
	this.ctime = ctime;
	this.plugin = plugin;
	this.enable = enable;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getEvent() {
	return this.event;
    }

    public void setEvent(String event) {
	this.event = event;
    }

    public String getDescri() {
	return this.descri;
    }

    public void setDescri(String descri) {
	this.descri = descri;
    }

    public String getRetype() {
	return this.retype;
    }

    public void setRetype(String retype) {
	this.retype = retype;
    }

    public Integer getMsgId() {
	return this.msgId;
    }

    public void setMsgId(Integer msgId) {
	this.msgId = msgId;
    }

    public String getEventkey() {
	return this.eventkey;
    }

    public void setEventkey(String eventkey) {
	this.eventkey = eventkey;
    }

    public Timestamp getCtime() {
	return this.ctime;
    }

    public void setCtime(Timestamp ctime) {
	this.ctime = ctime;
    }

    public String getPlugin() {
	return this.plugin;
    }

    public void setPlugin(String plugin) {
	this.plugin = plugin;
    }

    public Boolean getEnable() {
	return this.enable;
    }

    public void setEnable(Boolean enable) {
	this.enable = enable;
    }

}