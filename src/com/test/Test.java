package com.test;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpUtils;

import junit.framework.TestCase;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weixin.exception.WeixinException;
import com.weixin.service.WeiXinService;
import com.weixin.service.WeiXinServiceImpl;
import com.weixin.util.HttpUtil;
import com.weixin.util.WeixinMsg;
import com.weixin.vo.WeixinMenu;

public class Test extends TestCase {
    public Test(String name) {
	super(name);
	// TODO Auto-generated constructor stub
    }
    WeiXinService service = new WeiXinServiceImpl();

    /**
     * 配置文件测试
     */
    public void Configtest() {
	String[] keys = { "appID", "appsecret", "Token", "grant_type",
		"partnerKey" };
	for (String key : keys) {
	    System.out.println(service.get(key));
	}

    }
    /**
     * 获取AccessToken
     * 
     * @throws WeixinException
     */
    public void AccessTokenTest() throws WeixinException {
	System.out.println(service.getAccessToken());
    }

    /**
     * 菜单测试
     * 
     * @throws WeixinException
     */
    public void Menutest() throws WeixinException {
	JSONObject object = service.findMenu();
	for (Entry<String, Object> obj : object.entrySet()) {
	    System.out.println(obj.getKey() + ":" + obj.getValue());
	}

	JSONObject menuobj = object.getJSONObject("menu");

	if (menuobj != null) {
	    String array = menuobj.getString("button");
	    if (array != null) {
		List<WeixinMenu> list = JSON
			.parseArray(array, WeixinMenu.class);
		for (WeixinMenu weixinMenu : list) {
		    System.out.println("------------------------------");
		    System.out.println("name:" + weixinMenu.getName());
		    System.out.println("type" + weixinMenu.getType());
		    System.out.println("url:" + weixinMenu.getUrl());
		    System.out.println("key:" + weixinMenu.getKey());
		    List<WeixinMenu> list2 = weixinMenu.getSub_button();
		    System.out.println("子菜单");
		    for (WeixinMenu weixinMenu2 : list2) {
			System.out.println("-------------");
			System.out.println("name:" + weixinMenu2.getName());
			System.out.println("type" + weixinMenu2.getType());
			System.out.println("url:" + weixinMenu2.getUrl());
			System.out.println("key:" + weixinMenu2.getKey());
		    }
		}
	    }
	}

    }

    /**
     * 用户组组测试
     * 
     * @throws WeixinException
     */
    public void UserGroupTest() throws WeixinException {
	JSONObject object = service.findAllGroups();
	for (Entry<String, Object> obj : object.entrySet()) {
	    System.out.println(obj.getKey() + ":" + obj.getValue());
	}
    }

    /**
     * 获取关注用户列表以及用户信息
     * 
     * @throws WeixinException
     */
    public void UserListTest() throws WeixinException {
	JSONObject object = service.getFollwersList("");
	int total = object.getIntValue("total");
	System.out.println("总关注人数+" + total);

	JSONArray openids = object.getJSONObject("data").getJSONArray("openid");
	System.out.println(openids.size());
	Iterator<Object> iterator = openids.iterator();
	while (iterator.hasNext()) {
	    String oprnid = iterator.next().toString();
	    try {
		JSONObject object2= service.getUserInfo(oprnid);
		System.out.println(object2.toJSONString());
	    } catch (Exception e) {
		System.out.println(e);
		continue;

	    }
	}
    }
    /**
     * 临时二维码测试
     * @throws WeixinException
     */
    public void createSceneTest() throws WeixinException{
	JSONObject object= service.createScene(1800, 222);
	System.out.println(object.toJSONString());
    }
    /**
     * 获取用户信息
     * @throws WeixinException
     */
    public void testa() throws WeixinException{
	JSONObject object = service.getUserInfo("oRK-euAAUiB-d7qgpGaDQLWi4Sx0");
	System.out.println(object.toJSONString());
    }
    
    
    public void test() throws WeixinException{
	File file=new File("E:/1.jpg");
	JSONObject jsonObject=service.fileUpload(file, WeixinMsg.image);
	System.out.println(jsonObject.toJSONString());
    }

}