package com.service;

import java.util.Map;

import com.entity.Msgprs;
/**
 * 
 * MsgSendServer.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午1:14:23 
 * @version
 * @user micrxdd
 */
public interface MsgSendServer {
    /**
     * 向用户发送消息
     * @param msgprs
     * @param msgReceived
     * @return 消息
     */
	public Object SendMsg(Msgprs msgprs,Map<String, String> msg);
	/**
	 * 发送默认消息
	 * @param msgReceived
	 * @return 消息
	 */
	public Object SendDefaultMsg(Map<String, String> msg);
}
