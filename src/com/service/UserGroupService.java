package com.service;

import java.util.List;

import com.dto.Pageinfo;
import com.dto.Pagers;
import com.dto.UserGroupDto;
/**
 * 用户组服务接口
 * UserGroupService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午3:00:01 
 * @version
 * @user micrxdd
 */
public interface UserGroupService {
    /**
     * 返回用户组列表
     * @param pageinfo
     * @return
     */
    public Pagers Groups(Pageinfo pageinfo);
    /**
     * 保存用户组
     * @param dtos
     */
    public void SaveGroups(List<UserGroupDto> dtos);
    /**
     * 更新用户组
     * @param dtos
     */
    public void UpdateGroups(List<UserGroupDto> dtos);
    /**
     * 更新用户组规则
     * @param groupid
     * @param ids
     */
    public void UpdateRole(Integer groupid,String ids);
    /**
     * 删除用户组
     * @param ids
     */
    public void DelGroups(Integer[] ids);
}
