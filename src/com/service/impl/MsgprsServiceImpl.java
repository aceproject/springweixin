package com.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.MsgprsDao;
import com.dto.MsgprsDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.Msgprs;
import com.service.MsgprsService;
@Service("msgprsService")
public class MsgprsServiceImpl implements MsgprsService {
    @Resource
    private MsgprsDao msgprsDao;
    @Override
    public Pagers MsgprsList(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	Pagers pagers=msgprsDao.getForPage(pageinfo);
	List<Msgprs> msgprs=(List<Msgprs>) pagers.getRows();
	List<MsgprsDto> dtos=new ArrayList<MsgprsDto>();
	for (Msgprs msgprs2 : msgprs) {
	    MsgprsDto dto=new MsgprsDto();
	    BeanUtils.copyProperties(msgprs2, dto);
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }

    @Override
    public void SaveMsgprs(List<MsgprsDto> dtos) {
	// TODO Auto-generated method stub
	for (MsgprsDto msgprsDto : dtos) {
	    Msgprs msgprs=new Msgprs();
	    BeanUtils.copyProperties(msgprsDto, msgprs);
	    msgprsDao.save(msgprs);
	}
    }

    @Override
    public void UpdateMsgprs(List<MsgprsDto> dtos) {
	// TODO Auto-generated method stub
	for (MsgprsDto msgprsDto : dtos) {
	    
	    if(msgprsDto.getId()<0){
		Msgprs msgprs=new Msgprs();
		BeanUtils.copyProperties(msgprsDto, msgprs);
		msgprsDao.save(msgprs);
	    }else{
		Msgprs msgprs=msgprsDao.findById(msgprsDto.getId());
		BeanUtils.copyProperties(msgprsDto, msgprs);
	    }
	}
    }

    @Override
    public void DelMsgprs(String ids) {
	// TODO Auto-generated method stub
	msgprsDao.DelByStringids(ids);
    }

    @Override
    public void DelMsgIntprs(Integer[] ids) {
	// TODO Auto-generated method stub
	msgprsDao.DelByIntids(ids);
    }

}
