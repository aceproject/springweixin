package com.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dao.EventDao;
import com.dao.MsgroleDao;
import com.dao.TextDao;
import com.dao.TextroleDao;
import com.entity.Msgrole;
import com.entity.Textrole;
import com.service.MsgSendServer;
import com.service.MsgService;
import com.weixin.msg.MsgEmum;
import com.weixin.plugin.MsgPlugin;
import com.weixin.util.WeixinMsg;
/**
 * 消息服务类
 * MsgServiceImpl.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午12:43:15 
 * @version
 * @user micrxdd
 */
@Service("msgService")
public class MsgServiceImpl implements MsgService {
    // 规则列表
    private Map<String, Msgrole> map = new HashMap<String, Msgrole>();
    @Resource
    private MsgSendServer msgSendServer;
    @Resource
    private MsgPlugin textMsgPlue;
    @Resource
    private MsgroleDao MsgroleDao;
    @Resource
    private TextroleDao textroleDao;
    @Resource
    private TextDao textDao;
    @Resource
    private EventDao eventDao;
    // 关键词列表
    private List<Textrole> textkeys;

    @Override
    public Object onMsgReveived(Map<String, String> msg) {
	// TODO Auto-generated method stub
	Object out = null;

	try {
	    MsgEmum msgEmum = MsgEmum.valueOf(msg.get(WeixinMsg.MsgType));
	    switch (msgEmum) {

	    case text:
		System.out.println("接受到文yy本消息");
		out = onTextMsg(msg);
		break;
	    case event:
		System.out.println("接受到事件s消息");
		out = onEventMsg(msg);
		break;
	    default:
		System.out.println("接受到其他消息");
		out = onOtherMsg(msg);
		break;
	    }
	    return out;
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return null;
    }

    // 关键词回复目前只是简单的通过遍历每个关键词
    public Object onTextMsg(Map<String, String> msg) {
	if (textkeys == null) {
	    TextKeyListInit();
	}
	String textmsg = msg.get(WeixinMsg.Content);
	String key = null;
	for (Textrole textkey : textkeys) {
	    key = textkey.getTxtkey();
	    if (textkey.getIsstart()) {
		if (textmsg.startsWith(key)) {
		    return msgSendServer.SendMsg(textkey.getMsgprs(),
			    msg);
		}
	    } else {
		if (textmsg.indexOf(key) != -1) {
		    return msgSendServer.SendMsg(textkey.getMsgprs(),
			    msg);
		}
	    }
	}
	return msgSendServer.SendDefaultMsg(msg);
    }

    // 处理事件消息
    public Object onEventMsg(Map<String, String> msg) {
	// TODO Auto-generated method stub
	// DeBugUtil.log(eventMsg);
	if (map.size() == 0) {
	    MsgRolesInit();
	}
	System.out.println("--------------------");
	Set<String> iterator=map.keySet();
	for (String string : iterator) {
	    System.out.println(string);
	}
	Msgrole msgrole = null;
	if (msg.get(WeixinMsg.EVENT).equals(WeixinMsg.CLICK)) {
	    msgrole = map.get(msg.get(WeixinMsg.EventKey));
	} else {
	    msgrole = map.get(msg.get(WeixinMsg.EVENT));
	}
	if (msgrole != null) {
	    return msgSendServer.SendMsg(msgrole.getMsgprs(), msg);
	}
	return msgSendServer.SendDefaultMsg(msg);
    }

    public Object onOtherMsg(Map<String, String> msg) {
	if (map.size() == 0) {
	    MsgRolesInit();
	}
	Msgrole msgrole = map.get(msg.get(WeixinMsg.MsgType));
	if (msgrole != null) {
	    return msgSendServer.SendMsg(msgrole.getMsgprs(), msg);
	}
	return msgSendServer.SendDefaultMsg(msg);

    }

    // 初始化相关参数
    @Override
    public void MsgRolesInit() {
	// TODO Auto-generated method stub
	TextKeyListInit();
	MsgRoleInit();
    }

    // 初始化关键词列表
    public void TextKeyListInit() {
	textkeys = textroleDao.findAll();
    }

    // 初始化消息处理器
    public void MsgRoleInit() {
	map.clear();
	List<Msgrole> Msgroles = MsgroleDao.findAll();
	// 为了方便处理，分开普通消息和事件消息
	for (Msgrole msgrole : Msgroles) {
	    if (msgrole.getRtype().equals(WeixinMsg.EVENTCase)) {
		if (msgrole.getEvent().equals(WeixinMsg.CLICK)) {
		    map.put(msgrole.getEventkey(), msgrole);
		} else {
		    map.put(msgrole.getEvent(), msgrole);
		}
	    } else {
		map.put(msgrole.getRtype(), msgrole);
	    }
	}
    }

    @Override
    public Object onMsgReveived(String msg) {
	// TODO Auto-generated method stub
	return null;
    }

}
